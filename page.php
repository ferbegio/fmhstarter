<?php get_header(); ?>
<div class="content">
	<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
	<div class="page-title">
		<div class="container">
			<h1><?php the_title(); ?></h1>
		</div>
	</div>
	<div class="container">
		<?php the_content(); ?>
	</div>
</div>
	
	

<?php endwhile; else: ?>
	<p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
<?php endif; ?>
<?php get_footer(); ?>