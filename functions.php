<?php 

function wpbootstrap_scripts_with_jquery()
{
	// Register the script like this for a theme:
	wp_register_script( 'custom-script', get_template_directory_uri() . '/js/bootstrap.js', array( 'jquery' ) );
	// For either a plugin or a theme, you can then enqueue the script:
	wp_enqueue_script( 'custom-script' );
}
add_action( 'wp_enqueue_scripts', 'wpbootstrap_scripts_with_jquery' );


// Abilitazione delle immagini dei post
add_theme_support( 'post-thumbnails' );

// definizione di formato immagine per homepage
add_image_size( $name = 'home_thumb', $width = 217, $height = 217, $crop = true );

// Aggiunta supporto per custom menus
add_theme_support( 'menus' );

// Lunghezza anteprima testo
function custom_excerpt_length( $length ) {
	return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

// Rimozione dei [...]
function new_excerpt_more( $more ) {
	return '';
}
add_filter('excerpt_more', 'new_excerpt_more');

// niente tag nell excerpt
remove_filter('the_excerpt', 'wpautop');

// aggiunta widget al footer
if (function_exists('register_sidebar')) {

	register_sidebar(array(
		'name' => 'Footer1',
		'id'   => 'Footer1',
		'description'   => 'This is the widgetized header.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>'
	));
	register_sidebar(array(
		'name' => 'Footer2',
		'id'   => 'Footer2',
		'description'   => 'This is the widgetized header.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>'
	));
	register_sidebar(array(
		'name' => 'Footer3',
		'id'   => 'Footer3',
		'description'   => 'This is the widgetized header.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>'
	));
	register_sidebar(array(
		'name' => 'Footer4',
		'id'   => 'Footer4',
		'description'   => 'This is the widgetized header.',
		'before_widget' => '<div id="%1$s" class="widget %2$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h6>',
		'after_title'   => '</h6>'
	));
	
}


?>