<?php get_header(); ?>
<div class="Slider">
	<div class="container">
		<h1>La tradizione sarda</h1>
		<h2>LLa mia terra, la Sardegna, è variegata, diversificata e caratterizzata dall'insularità e dalla cultura agropastorale .<br><br>Da questa terra ho ereditato i sapori più delicati, più marcati, ma sempre inconfondibili e graditi al palato. La tradizione gastronomica sarda è semplice ed austera, ha gusti, sapori e odori il più delle volte insoliti</h2>
		<a class="btn btn-default btn-lg" href="/category/ricette/">Scopri le mie ricette</a>
	</div>
</div>
<div class="content">
<div class="page-title">
	<div class="container">
		<h3>Dal blog di Daniele</h3>
	</div>
</div>
<div class="container">
<div class="row">
	<?php $the_query = new WP_Query( 'showposts=4' ); ?>
	<?php while ($the_query -> have_posts()) : $the_query -> the_post(); ?>
	
		<div class="col-sm-3">
			<a href="<?php the_permalink() ?>" class="post-anchor">
				<?php the_post_thumbnail('home_thumb', array('class' => 'img-responsive')); ?>
				<h4>
			<a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h4>
			<span><?php the_excerpt(); ?></span>
			<a class="btn btn-default btn-black btn-xs pull-right" href="<?php the_permalink() ?>">Leggi tutto</a>
			</div>
			<?php endwhile;?>
		
	</div>
</div>
<?php get_footer(); ?>



