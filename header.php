<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php wp_title('|',1,'right'); ?> <?php bloginfo('name'); ?></title>

    <!-- Bootstrap -->
    <link href="<?php bloginfo('stylesheet_url');?>" rel="stylesheet">
   
    

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    <?php wp_enqueue_script("jquery"); ?>
    <?php wp_head(); ?>
  </head>
  <body>
  <nav class="navbar navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="<?php echo site_url(); ?>"><img src="http://www.chefdanielesanna.com/wp-content/uploads/2014/06/logo-nav-sanna.png" alt="<?php bloginfo('name'); ?>"/></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
     <?php wp_nav_menu( array( 
  	'theme_location' 	  => '',
  	'container' 		  => 'div',
  	'container_class' 	=> 'collapse navbar-collapse',
  	'container_id'    	=> 'main-navbar-collapse',
  	'menu_class'      	=> 'nav navbar-nav',
  	'menu_id'         	=> '',
  	'echo'            	=> true,
  	'fallback_cb'     	=> 'wp_page_menu',
  	'before'          	=> '',
  	'after'           	=> '',
  	'link_before'     	=> '',
  	'link_after'      	=> '',
  	'items_wrap'      	=> '<ul id="%1$s" class="%2$s">%3$s</ul>',
  	'depth'           	=> 0,
  	'walker'          	=> ''
    )); ?>
  </div><!-- /.container-fluid -->
</nav>
 