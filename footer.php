    <div class="footer">
	    <div class="container">
		    <div class="row">
			    <div class="col-sm-3">
				    <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer1')) : else : ?>

	<div class="pre-widget">
		<p><strong>Widgetized Sidebar</strong></p>
		<p>This panel is active and ready for you to add some widgets via the WP Admin</p>
	</div>

	<?php endif; ?>
			    </div>
			    <div class="col-sm-3">
				     <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer2')) : else : ?>

	<div class="pre-widget">
		<p><strong>Widgetized Sidebar</strong></p>
		<p>This panel is active and ready for you to add some widgets via the WP Admin</p>
	</div>

	<?php endif; ?>
			    </div>
			    <div class="col-sm-3">
				     <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer3')) : else : ?>

	<div class="pre-widget">
		<p><strong>Widgetized Sidebar</strong></p>
		<p>This panel is active and ready for you to add some widgets via the WP Admin</p>
	</div>

	<?php endif; ?>
			    </div>
			    <div class="col-sm-3">
				     <?php if (function_exists('dynamic_sidebar') && dynamic_sidebar('Footer4')) : else : ?>

	<div class="pre-widget">
		<p><strong>Widgetized Sidebar</strong></p>
		<p>This panel is active and ready for you to add some widgets via the WP Admin</p>
	</div>

	<?php endif; ?>
			    </div>
		    </div>
	    </div>
	    <div class="copyright">
		    <div class="container">
			    <small>Copyright &copy; 2014 Chef Daniele Sanna</small>
			    <small class="pull-right">Design & coding <a href="http://www.fisheyemediahouse.com">Fisheye media house</a></small>
		    </div>
	    </div>
    </div>
  <?php wp_footer(); ?>

  </body>
</html>