<?php get_header(); ?>

<div class="content">
	
	<div class="page-title">
		<div class="container">
			<h1>Il blog di Daniele Sanna</h1>
		</div>
	</div>

 <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
 <div class="row">
		<div class="container">
			<a href="<?php the_permalink(); ?>" class="post-anchor">
				<div class="col-sm-4">
					<?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?>
				</div>
		<div class="col-sm-8">		
    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a><small class="pull-right"><?php the_time('j F Y'); ?></small></h2>
    <p><?php the_excerpt(); ?></p>
					<a href="<?php the_permalink(); ?>" class="btn btn-default btn-black btn-xs pull-right">Leggi tutto</a>
   </div>
			</a>
			
		</div>
		<hr>
	</div>
  
  
  <?php endwhile; else: ?>
      <p><?php _e('Sorry, there are no posts.'); ?></p>
    <?php endif; ?>
  

	
</div>



  
  
    <?php get_footer(); ?>
